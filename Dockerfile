FROM golang:1.10.0

WORKDIR /{Enter-Project-Folder}

COPY . .

RUN go get -d -v ./...

RUN go install -v ./...

ENTRYPOINT ["app"]